#!/bin/bash 
#RUTAS empresa se pondra donde se va guardar en el contenedor 
#-v empresas es para hacer el volumen persistente de empresas

docker run --name uaaxml2pdf  -e TZ=America/Mexico_City  --env RUTA_EMPRESAS=/empresas \
  -v empresas:/empresas -p8080:8080 -d fggn/uaaxml2cfdi

#docker cp empresas xml2pdf:/
