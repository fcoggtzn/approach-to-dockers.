#!/usr/bin/env bash
# ==============================================
# NETWORK
# Detect if evpl network exist if not create it
# ==============================================


NETWORK_NAME="evpl"
DOCKER_NET_EXIST=$(docker network ls --no-trunc --quiet --filter "name=$NETWORK_NAME")

echo " -- Checking custom docker network -- "

if [ -n "$DOCKER_NET_EXIST" ]; then
    printf "Active Network: $NETWORK_NAME\n"
else
    printf "Creating Network $NETWORK_NAME\n"
    docker network create evpl
fi

exit 0
