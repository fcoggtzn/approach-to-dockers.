#!/bin/bash 

docker run -d --rm --name mysql -e MYSQL_ROOT_PASSWORD=root -p 3306:3306 -v mysql_data:/var/lib/mysql mysql:5.7.22

sleep 30 

#cargar datos a mysql
docker exec  -i mysql   /usr/bin/mysql   -uroot -proot < basedatos.sql  
docker exec  -i mysql   /usr/bin/mysql sole3   -uroot -proot < respaldo.sql  

